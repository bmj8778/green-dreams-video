﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateCamera : MonoBehaviour {

    Vector3 rot;

    Vector3 maxRot;
    Vector3 minRot;
    float length;

	// Use this for initialization
	void Start () {
        rot = new Vector3(0, 0, 0);

        //length of video in seconds
        length = 280;

        //minimum and maximum rotation speed(in degrees per second)
        minRot = new Vector3(0,0,00);
        maxRot = new Vector3(0,0,15);
	}
	
	// Update is called once per frame
	void Update () {
        //rotate around z over time.
        Vector3 currRot = Vector3.Lerp(minRot, maxRot, Time.time / length);
        //Debug.Log(currRot);
        transform.Rotate(currRot*Time.deltaTime);
	}
}
