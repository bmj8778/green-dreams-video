﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doFade : MonoBehaviour {

    bool didIFadeYet;

	// Use this for initialization
	void Start () {
        didIFadeYet = false;
	}
	
	// Update is called once per frame
	void Update () {
	    //if enough time passes (TODO: length of video)
        //then start fade	
        if (Time.timeSinceLevelLoad > 4f && !didIFadeYet)
        {
            AutoFade.LoadScene(1, 2.5f, .25f, Color.black);
            didIFadeYet = true;
        }
	}
}
