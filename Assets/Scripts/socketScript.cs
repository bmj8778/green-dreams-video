﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;



public class socketScript : MonoBehaviour
{
    //variables
    private TCPConnection myTCP;
    private string serverMsg;
    public string msgToServer;

    void Awake()
    {
        //add a copy of TCPConnection to this game object
        myTCP = gameObject.AddComponent<TCPConnection>();
    }

    void Start()
    {

    }

    void Update()
    {
        //keep checking the server for messages, if a message is received from server, it gets logged in the Debug console (see function below)
        SocketResponse();
    }

    void OnGUI()
    {
        
    }
    //socket reading script

    void SocketResponse()
    {

        string serverSays = myTCP.readSocket();

        if (serverSays != "")
        {
            Debug.Log("[SERVER]" + serverSays);
        }
    }



    //send message to the server

    public void SendToServer(string str)
    {
        myTCP.writeSocket(str);
        Debug.Log("[CLIENT] -> " + str);

    }
}
