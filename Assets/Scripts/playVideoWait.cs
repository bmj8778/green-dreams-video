﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playVideoWait : MonoBehaviour {

    public UnityEngine.Video.VideoPlayer vp;
    private bool startedVideo;
    private AudioSource audioSource;

	// Use this for initialization
	void Start () {
        startedVideo = false;
        audioSource = gameObject.AddComponent<AudioSource>();


	}
	
	// Update is called once per frame
	void Update () {
        float test = Time.time;
        if (!startedVideo && test > 0.0f){
            vp.EnableAudioTrack(0, true);
            vp.SetTargetAudioSource(0, audioSource);

            vp.Prepare();

            vp.Play();
            startedVideo = true;
        }

        //Debug.Log(test);
	}
}
