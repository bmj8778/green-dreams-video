﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class PipeSystem : MonoBehaviour {

	public Pipe pipePrefab;

    public Pipe pipePrefab1;

    public Pipe pipePrefab2;

    public Pipe pipePrefab3;

    public Pipe pipePrefab4;

    public int pipeCount;

    private  List<MovieTexture> textureList;

    private List<string> folderList;

    private int texIndex;

	public Pipe[] pipes;

    public void loadTextures() {
        //TODO: finialize plan of attack. Currently 
        //I will first loop through the Resources folder, and find all of the folders.
        //Each folder corresponds to an animation! We then pass this string to generate.
        //Each pipe will load the resources with loadAll using that String name!

        //TODO: this is hardcoded and not good
        //string[] path = Application.dataPath.Split(@"/".ToCharArray());
        //string pathToAnimations = "";
        //bool firstTime = true;

        //for (int i = 0; i < path.Length; i++)
        //{
        //    if (firstTime)
        //    {
        //        pathToAnimations += path[i];
        //        firstTime = false;
        //    }else
        //    {
        //        pathToAnimations += System.IO.Path.DirectorySeparatorChar + path[i];
        //    }

        //}

        //pathToAnimations += System.IO.Path.DirectorySeparatorChar + "Animations";

        string pathToAnimations = Application.dataPath + @"/Resources";
        string[] folders = System.IO.Directory.GetDirectories(pathToAnimations);

        folderList = new List<string>();
        texIndex = 0;

        //put in list
        for (int i = 0; i < folders.Length; i++) {
            string folder = folders[i];
            string folderName = folder.Split(System.IO.Path.DirectorySeparatorChar).Last();
            folderList.Add(folderName);
        }

        //TODO: food for thought if we are going to do the intensity thing,
        //Have equal numbers images, then choose which set to pick from based on intensity!!!

        //randomize list
        System.Random rnd = new System.Random();
        for (int i = folderList.Count; i > 1; i--) {
            int pos = rnd.Next(i);
            var x = folderList[i - 1];
            folderList[i - 1] = folderList[pos];
            folderList[pos] = x;
        }

    }

    public void loadMovieTextures()
    {
        //Initially load all of the .ogv files inside the Resources folder

        //get all mp4s 
        MovieTexture[] objs = Resources.LoadAll("", typeof(MovieTexture)).Cast<MovieTexture>().ToArray();

        textureList = new List<MovieTexture>();
        texIndex = 0;
        //put in list
        for (int i = 0;i < objs.Length; i++)
        {
            textureList.Add(objs[i]);
        }

        //randomize
        System.Random rnd = new System.Random();
        for (int i = textureList.Count; i > 1; i--) {
            int pos = rnd.Next(i);
            var x = textureList[i - 1];
            textureList[i - 1] = textureList[pos];
            textureList[pos] = x;
        }

    }

	private void Awake () {
        loadTextures();

        //TODO: clean up hardcoded
        pipes[0].Generate();
        pipes[1].Generate();
        pipes[2].Generate();
        pipes[3].Generate();


        //pipes = new Pipe[pipeCount];
        //for (int i = 0; i < pipes.Length; i++) {
        //    Pipe pipe = pipes[i] = Instantiate<Pipe>(pipePrefab);
        //    pipe.transform.SetParent(transform, false);
        //    if (i > 0) {
        //        pipe.Generate(getTexture(), getTexture());
        //        pipe.AlignWith(pipes[i - 1]);
        //    }
        //}
        //AlignNextPipeWithOrigin();

        //pipes = new Pipe[pipeCount];

        //Pipe pipe1 = pipes[0] = Instantiate<Pipe>(pipePrefab1);
        //Pipe pipe2 = pipes[1] = Instantiate<Pipe>(pipePrefab2);
        //Pipe pipe3 = pipes[2] = Instantiate<Pipe>(pipePrefab3);
        //Pipe pipe4 = pipes[3] = Instantiate<Pipe>(pipePrefab4);

        //pipe1.transform.SetParent(transform, false);
        //pipe2.transform.SetParent(transform, false);
        //pipe3.transform.SetParent(transform, false);
        //pipe4.transform.SetParent(transform, false);

        //pipe2.AlignWith(pipes[0]);
        //pipe3.AlignWith(pipes[1]);
        //pipe4.AlignWith(pipes[2]);

        //AlignNextPipeWithOrigin();
    }
    //Pull path to folder containing image sequence of textures
    //TODO: go until end, then repeat last, for now.
    private string getTexture()
    {
        if (texIndex >= folderList.Count)
        {
            texIndex = 0;
        }
        string ret = folderList[texIndex];
        texIndex++;
        return ret;
    }

    public Pipe SetupFirstPipe () {
		transform.localPosition = new Vector3(0f, -pipes[1].CurveRadius);
        pipes[1].playMovies();
        pipes[2].playMovies();
        pipes[3].playMovies();
		return pipes[1];
	}

	public Pipe SetupNextPipe () {
		ShiftPipes();
		AlignNextPipeWithOrigin();
		pipes[pipes.Length - 1].Generate(getTexture(), getTexture());
		pipes[pipes.Length - 1].AlignWith(pipes[pipes.Length - 2]);
		transform.localPosition = new Vector3(0f, -pipes[1].CurveRadius);
        pipes[1].playMovies();
        pipes[2].playMovies();
		return pipes[1];
	}

	private void ShiftPipes () {
		Pipe temp = pipes[0];
		for (int i = 1; i < pipes.Length; i++) {
			pipes[i - 1] = pipes[i];
            
		}
        
        temp.stopMovies();
        pipes[0].stopMovies();

        pipes[pipes.Length - 1] = temp;
	}

	private void AlignNextPipeWithOrigin () {
		Transform transformToAlign = pipes[1].transform;
		for (int i = 0; i < pipes.Length; i++) {
			if (i != 1) {
				pipes[i].transform.SetParent(transformToAlign);
			}
		}
		
		transformToAlign.localPosition = Vector3.zero;
		transformToAlign.localRotation = Quaternion.identity;
		
		for (int i = 0; i < pipes.Length; i++) {
			if (i != 1) {
				pipes[i].transform.SetParent(transform);
			}
		}
	}
}