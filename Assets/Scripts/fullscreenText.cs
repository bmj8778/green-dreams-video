﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fullscreenText : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    public Texture backgroundTexture;

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
