﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doFade2 : MonoBehaviour {

    bool didIFadeYet;

	// Use this for initialization
	void Start () {
        didIFadeYet = false;
	}
	
	// Update is called once per frame
	void Update () {
        //then start fade	
        if (Time.timeSinceLevelLoad > 226.5f && !didIFadeYet)
        {
            AutoFade.LoadScene(2, 1.5f, .25f, Color.black);
            didIFadeYet = true;
            Debug.Log("faded");
        }
	}
}
