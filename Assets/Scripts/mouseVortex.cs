﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace UnityStandardAssets.ImageEffects
{
    //[ExecuteInEditMode]
    //[AddComponentMenu("Image Effects/Displacement/Vortex")]
    public class mouseVortex : ImageEffectBase
    {
        public Vector2 radius = new Vector2(0.4F, 0.4F);
        public float angle = 50;
        public Vector2 center = new Vector2(0.5F, 0.5F);
        TCPConnection tcpConnection;
        bool isConnected;
        float rThresh;
        float minAngle;
        float maxAngle;
        float minR;
        float maxR;
        float prevR;
        List<Vector2> points;
        List<Vector2> radiuses;
        List<float> angles;
        //we lerp from currPoint to nextPoint
        Vector2 currPoint;
        Vector2 nextPoint;
        Vector2 currR;
        Vector2 nextR;
        float currAngle;
        float nextAngle;

        float lerpVal;
        float length;

        //TODO: try lerping angle with second hand for now

        float lerpSpeed;
            
        private void Awake()
        {
            tcpConnection = gameObject.AddComponent<TCPConnection>();
        }

        private void Start()
        {
            rThresh  = 0.05f;
            minR     = 0.0f;
            minAngle = 0.0f;
            maxR     = 0.1f;
            maxAngle = 25f;
            prevR    = 0f;
            lerpSpeed= 1.6f;
            lerpVal  = 1f;

            currPoint = new Vector2(0, 0);
            nextPoint = new Vector2(0, 0);

            nextPoint = new Vector2(0, 0);
            nextR     = new Vector2(0, 0);

            currAngle = 0f;
            nextAngle = 0f;
            

            points = new List<Vector2>();
            radiuses = new List<Vector2>();
            angles = new List<float>();

            //length of video in seconds
            length = 280;

            //set up socket
            //call set host and IP as needed
            tcpConnection.setupSocket();

            //check tcpConnection.socketReady, if not use mouse
            if (tcpConnection.isConnected && tcpConnection.mySocket.Connected)
            {
                isConnected = true;
            }else
            {
                isConnected = false;
            }
        }

        void Update()
        {
            //TODO: should we try to reconnect?

            if (isConnected)
            {
                //keep checking the server for messages, if a message is received from server, it gets logged in the Debug console (see function below)
                string serverString = SocketResponse();

                //Debug.Log(serverString);
               
                if (serverString != "")
                {
                    //first check if we got multiple hand positions.

                    string[] results = serverString.Split('(', ')');
                    //if not tracked hands skip and don't update variables
                    if (results.Length > 0)
                    {
                        //Debug.Log("tracking");
                        
                        //grab last entry in parentheses
                        string curr = results[results.Length - 2];

                        string[] elements = curr.Split(',');

                        //TODO: check how many tracked hands are here, check size of elements for validity
                        //We can work on scheme or just concatenate i.e. (r0,theta0, r1, theta1)
                        //we can divide length by 2 to check how many

                        //Debug.Log(results);

                        float r = float.Parse(elements[0]);
                        float theta = float.Parse(elements[1]);

                        //float r= 0f;
                        //float.TryParse(elements[0], out r);

                        //float theta = 0f;
                        //float.TryParse(elements[1], out theta);

                        //Debug.Log("r: " + r + "theta: " + theta);

                        //if pass threshold test then add to the list of point 
                        if (Math.Abs(r - prevR) > rThresh)
                        {

                            prevR = r;
                            //convert to x,y add .5 to each x and y to get between [0,1]
                            Vector2 newCenter = new Vector2();
                            Vector2 newRadius = new Vector2();

                            //TODO: do the intensities in the function where vortex is applied!
                            //float intensityR = Mathf.Lerp(maxR, minR, Time.time / length);

                            newCenter.x = (float)((r) * Math.Cos(theta * (Math.PI / 180)) + 0.5f);
                            newCenter.y = (float)((r) * Math.Sin(theta * (Math.PI / 180)) + 0.5f);

                            
                            newRadius.x = Mathf.Max(r,0.1f); //+0.2f;
                            newRadius.y = Mathf.Max(r, 0.1f);

                            //Debug.Log("r: " + newRadius + "c: " + newCenter + "angle: " + theta);

                            //newCenter.x += 0.6f;
                            //newCenter.y += 0.6f;
                            //if (newCenter.x > 0.5f) {
                            //    newCenter.x -= 0.2f;
                            //}else if (newCenter.x < 0.5f) {
                            //    newCenter.x += 0.2f;
                            //}

                            //if (newCenter.y > 0.5f) {
                            //    newCenter.y -= 0.2f;
                            //} else if (newCenter.y < 0.5f) {
                            //    newCenter.y += 0.2f;
                            //}
                            theta = Mathf.Min(theta, 110f);

                            //TODO: maybe have random chance of adding it so list doesn't grow so big, or up threshold
                            points.Add(newCenter);
                            radiuses.Add(newRadius);
                            angles.Add(theta);
                            //uses current track for angle value, if 2 hands uses other for angle.
                            //angle = theta;
                        }

                        //update current r and center
                        

                        //Debug.Log(lerpVal);
                        //Debug.Log("lerp " + lerpVal);
                        


                    }
                }
                lerpVal += Time.deltaTime * lerpSpeed;

                if (lerpVal >= 1f) {
                    if (points.Count > 0) {
                        currR = nextR;
                        currPoint = nextPoint;
                        currAngle = nextAngle;

                        nextR = radiuses[radiuses.Count - 1];
                        //radiuses.RemoveAt(0);

                        nextPoint = points[points.Count - 1];
                        //points.RemoveAt(0);

                        nextAngle = angles[angles.Count - 1];
                        //angles.RemoveAt(0);

                        center = currPoint;
                        radius = currR;
                        angle = currAngle;

                        radiuses.RemoveRange(0, radiuses.Count - 3);
                        points.RemoveRange(0, points.Count - 3);
                        angles.RemoveRange(0, angles.Count - 3);

                        lerpVal = 0.0f;
                        //Debug.Log("done lerping");
                    } else {
                        lerpVal = 0.0f;
                    }
                } else //else update current animation
                 {
                    center = Vector2.Lerp(currPoint, nextPoint, lerpVal);
                    radius = Vector2.Lerp(currR, nextR, lerpVal);
                    angle = Mathf.Lerp(currAngle, nextAngle, lerpVal);

                }

            } else
            {
                Vector3 mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);

                //going to try using mouse position for center
                center.x = mousePos.x;
                center.y = mousePos.y;

                //get angle from normalized position. first adjust
                mousePos.x -= 0.5f;
                mousePos.y -= 0.5f;


                //double theta = Math.Atan2(mousePos.y, mousePos.x);
                //theta *= (180.0 / Math.PI);

                //TODO: use second hand for theta, or randomize between like 10-60 degrees
                double theta = 40.0;

                //theta *= (-180.0 / Math.PI);
                //theta += 180.0f;
                //if (theta < 0.0)
                //    theta += 360.0;
                //Debug.Log(theta);
                angle = (float)Math.Abs(theta);

                //IMPORTANT TODOs
                // * Randomize intensity (r values). Get more intense as song goes on
                // * Randomize angle between range (can increase as time goes on or not)
                // * Process second tracked hand if working.
                // * Will get video with border around it from Dongni
                // * What will we do at the end of video? Fade to outro live video?
                //   - Update script to look at length of video and determine when to end.
                // * maybe move animations lower on pipe a bit
                // * outro, process second hand, interpolate between hand positions

                //assuming 5 different radius lengths, pick values of radius to set.
                double r = Math.Sqrt(Math.Pow(mousePos.x,2.0) + Math.Pow(mousePos.y, 2.0));



                radius.x = (float)Math.Abs(r) + 0.25f;
                radius.y = (float)Math.Abs(r) + 0.2f;

                angle = 66;
            }
        }

        //socket reading script

        string SocketResponse()
        {

            string serverSays = tcpConnection.readSocket();

            return serverSays;
        }

        // Called by camera to apply image effect
        void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            //Debug.Log("r: "+radius+"c: " + center + "angle: "+angle);

            //NO clamping here. Will clamp at time when data recieved
            ////clamp params
            //if (center.x >= 1.0f){
            //    center.x = 0.9f;
            //    //center.x -= 0.5f;
            //}
            //else if (center.x <= 0.0f) {
            //    center.x = 0.1f;
            //    //center.x += 0.5f;
            //}

            //if (center.y >= 1.0f) {
            //    center.y = 0.8f;
            //    //center.y -= 0.5f;
            //} else if (center.y <= 0.0f) {
            //    center.y = 0.2f;
            //    //center.y += 0.5f;
            //}

            //if (radius.x >= 1.0f) {
            //    radius.x = 0.5f;
            //    radius.y = 0.3f;
            //} else if (radius.x <= 0.1f) {
            //    radius.x = 0.15f;
            //    radius.y = 0.1f;
            //}

            
            //float intensityR = Mathf.Lerp(minR, maxR, Time.time / length);
            //radius.x += intensityR;
            //radius.x = (float) Math.Round(radius.x, 2);
            radius.x = Mathf.Min(radius.x, 1.0f);

            //radius.y += intensityR;
            //radius.y = (float)Math.Round(radius.y, 2);
            radius.y = Mathf.Min(radius.y, 1.0f);

            float intensityAngle = Mathf.Lerp(minAngle, maxAngle, Time.time / length);
            angle += intensityAngle;

            
            //Debug.Log("curr angle: " + currAngle + " next angle: " + nextAngle);

            angle = Mathf.Min(angle, 120f);
            //Debug.Log(lerpVal);

            ImageEffects.RenderDistortion(material, source, destination, angle, center, radius);
        }
    }
}
