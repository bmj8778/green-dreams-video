﻿using System.Linq;
using UnityEngine;

public class Pipe : MonoBehaviour {

    public bool initialized = false;

	public float pipeRadius;
	public int pipeSegmentCount;

    public Shader texShader;

    public Texture2D[] animation1;
    public Texture2D[] animation2;

    public bool isPlaying;

    float minRot = 0;
    float maxRot = 15;
    float length = 280;

    //these indicies represent which vertex in pipe to start each resource pane from
    public int bottomLeftIndex;
    public int topLeftIndex;
    public int bottomRightIndex;
    public int topRightIndex;

    public int bottomLeftIndex_2;
    public int topLeftIndex_2;
    public int bottomRightIndex_2;
    public int topRightIndex_2;

    public float frameTime = 1f/19f;
    public float elapsedTime = 0.0f;
    public int currentFrame1;
    public int currentFrame2;

    public Material[] materials;

    //m,n and o,p represent the tesselation level for resource 1 and 2 respectively each in different directions (vert,horiz)
    //These are found manually using gizmos draw sphere and guessing indicies from the pipeVerts.
    public int m;
    public int n;

    public int o;
    public int p;

    public float ringDistance;

	public float minCurveRadius, maxCurveRadius;
	public int minCurveSegmentCount, maxCurveSegmentCount;

	public float curveRadius;
	private int curveSegmentCount;

	private Mesh mesh;
	private Vector3[] vertices;
	private Vector2[] uv;
	private int[] triangles;

    int newListLen;
    int numPanelVerts;

    private float curveAngle;
	private float relativeRotation;

    private bool writeFile = true;

	public float CurveAngle {
		get {
			return curveAngle;
		}
	}

	public float CurveRadius {
		get {
			return curveRadius;
		}
	}

	public float RelativeRotation {
		get {
			return relativeRotation;
		}
	}

    private void Update()
    {
        if (isPlaying)
        {
            
            elapsedTime += Time.deltaTime;
            if (elapsedTime > frameTime)
            {
                //currentFrame1 += 1;
                //currentFrame2 += 1;
                currentFrame1 += (int) (elapsedTime/frameTime);
                currentFrame2 += (int)(elapsedTime / frameTime);

                elapsedTime = 0.0f;
            }

            //process each animation seperately as they are not all the same length.
            if (currentFrame1 >= animation1.Length)
            {
                currentFrame1 = 0;
            }

            if (currentFrame2 >= animation2.Length)
            {
                currentFrame2 = 0;
            }

            Material[] matCopy = this.GetComponent<MeshRenderer>().materials;

            matCopy[1].mainTexture = animation1[currentFrame1];
            matCopy[2].mainTexture = animation2[currentFrame2];

            this.GetComponent<MeshRenderer>().materials = matCopy;

            //rotate uv map for each resource, TODO: rotation is a fail for now
            float angle = Mathf.Lerp(minRot, maxRot, Time.time / length)*Time.deltaTime;

            Vector2 trans = new Vector2(-0.5f, -0.5f);

            for (int uvIndex = newListLen; uvIndex < newListLen + numPanelVerts; uvIndex++) {
                Vector2 curr = uv[uvIndex];
                curr += trans;
                //curr = Quaternion.Euler(0, angle, 0) * curr;

                float c = Mathf.Cos(-angle * Mathf.Deg2Rad);
                float s = Mathf.Sin(-angle * Mathf.Deg2Rad);

                curr = new Vector2(curr.x * c - curr.y * s,curr.x*s + curr.y*c);

                uv[uvIndex] = curr - trans;
            }

            for (int uvIndex = newListLen + numPanelVerts; uvIndex < newListLen + 2 * numPanelVerts; uvIndex++) {
                Vector2 curr = uv[uvIndex];
                curr += trans;
                //curr = Quaternion.Euler(0, angle, 0) * curr;

                float c = Mathf.Cos(-angle * Mathf.Deg2Rad);
                float s = Mathf.Sin(-angle * Mathf.Deg2Rad);

                curr = new Vector2(curr.x * c - curr.y * s, curr.x * s + curr.y * c);

                uv[uvIndex] = curr - trans;
            }

            ////mesh.uv = uv;

            mesh.uv = uv;
        }
    }

	private void Awake () {
		GetComponent<MeshFilter>().mesh = mesh = new Mesh();
		mesh.name = "Pipe";


	}

    private void writeVertsToFile(Vector3[] verts)
    {
        using (var writer = new System.IO.StreamWriter(System.IO.File.Create(@"C:\Users\Brendan\Desktop\testfile.txt")))
        {
            for (int i = 0; i < verts.Length; i++)
            {
                string temp = "";
                Vector3 curr = verts[i];
                temp += curr.x + ", " + curr.y + ", " + curr.z;
                writer.WriteLine(temp);
            }

        }



    }
    /**
    **Plays the two resource movie textures associated with this mesh
    */
    public void playMovies()
    {
        isPlaying = true;
    }

    public void stopMovies()
    {
        isPlaying = false;
    }

    public Texture2D[] getTexturesFromFolder(string s)
    {
        string[] files  = System.IO.Directory.GetFiles(s, "*.png");
        Texture2D[] ret = new Texture2D[files.Length];

        byte[] texData;
        Texture2D curr;
        for (int i = 0; i < ret.Length; i++)
        {
            texData = System.IO.File.ReadAllBytes(files[i]);
            curr = new Texture2D(2, 2, TextureFormat.BGRA32, false);
            curr.LoadImage(texData);
            
            ret[i] = curr;
        }

        return ret;
    }

    public void Generate()
    {
        isPlaying = false;
        curveRadius = Random.Range(minCurveRadius, maxCurveRadius);
        curveSegmentCount =
            Random.Range(minCurveSegmentCount, maxCurveSegmentCount + 1);
    }

    public void Generate (string t0, string t1) {
        isPlaying = false;
        curveRadius = Random.Range(minCurveRadius, maxCurveRadius);
		curveSegmentCount =
			Random.Range(minCurveSegmentCount, maxCurveSegmentCount + 1);
		//mesh.Clear();

        //m and n params for plane tesselation
        n = 2;
        m = 2;
        mesh.subMeshCount = 3;

        //only one vertices list. for triangle we have a seperate for each submesh
		SetVertices();
        //set uv in SetVertices() now.
		//SetUV();
		SetTriangles();
        mesh.RecalculateNormals();

        //TODO: test if we should just load one frame insetad of all at once.
        //Load list of textures from given animation folder.
        //animation1 = getTexturesFromFolder(t0);
        //animation2 = getTexturesFromFolder(t1);
        animation1 = Resources.LoadAll(t0, typeof(Texture2D)).Cast<Texture2D>().ToArray();
        animation2 = Resources.LoadAll(t1, typeof(Texture2D)).Cast<Texture2D>().ToArray();

        currentFrame1 = 0;
        currentFrame2 = 0;

        //TODO: make a noisy shader that uses pos.x and pos.z
        //      like current uv mapping does
        Material m0 = new Material(texShader);
        m0.mainTexture = animation1[currentFrame1];

        Material m1 = new Material(texShader);
        m1.mainTexture = animation2[currentFrame2];

        Material[] matCopy = this.GetComponent<MeshRenderer>().materials;

        matCopy[1] = m0;
        matCopy[2] = m1;
        
        this.GetComponent<MeshRenderer>().materials = matCopy;

        //t0.loop = true;
        //t1.loop = true;
        //t0.Play();
        //t1.Play();
    }

    private Vector3 findClosest(Vector3 v,Vector3[] verts)
    {
        //TODO: if check, verts length > 1

        float minDistance = Mathf.Infinity;
        Vector3 closest = new Vector3(0.0f,0.0f,0.0f);
        Vector3 curr;
        float currDistance;

        for (int i = 0; i < verts.Length; i++)
        {
            curr = verts[i];
            currDistance = Vector3.Distance(curr, v);
            if (currDistance < minDistance)
            {
                closest = verts[i];
                minDistance = currDistance;
            }
        }

        return closest;
    }

    private Vector3 bilerp(Vector3 a, Vector3 b, Vector3 c, Vector3 d,float u, float v)
    {
        Vector3 ret = (a) *(1.0f - u)*(1.0f - v) + b * (1.0f - u)*(v) + c * (u)*(v) + d * (u)*(1.0f - v);
        return ret;
    }

    private void SetPanelVertices()
    {
        Vector3 bottomLeft = vertices[bottomLeftIndex];
        Vector3 topLeft = vertices[topLeftIndex];
        Vector3 bottomRight = vertices[bottomRightIndex];
        Vector3 topRight = vertices[topRightIndex];

        //Vector3 bottomLeft = new Vector3(1.521749f, 20.25192f, -0.9510565f);
        //Vector3 topLeft = new Vector3(3.130887f, 20.7158f, 0.309017f);
        //Vector3 bottomRight = new Vector3(1.569857f, 20.89216f, -0.3090169f);
        //Vector3 topRight = new Vector3(3.076601f, 20.35661f, 0.809017f);

        //add 4 verts to mesh.vertices
        vertices[vertices.Length - 4] = bottomLeft;
        vertices[vertices.Length - 3] = topLeft;
        vertices[vertices.Length - 2] = bottomRight;
        vertices[vertices.Length - 1] = topRight;

        mesh.vertices = vertices;
    }

	private void SetVertices () {
        //length is lenth of pipe verts, plus ones depending on area of each resource plane
		vertices = new Vector3[(pipeSegmentCount * curveSegmentCount * 4) + 2*4*(m+1)*(n+1) ];

		float uStep = ringDistance / curveRadius;
		curveAngle = uStep * curveSegmentCount * (360f / (2f * Mathf.PI));
		CreateFirstQuadRing(uStep);
		int iDelta = pipeSegmentCount * 4;

        //bjohn: want to add resource at some point along quad ring
		for (int u = 2, i = iDelta; u <= curveSegmentCount; u++, i += iDelta) {
			CreateQuadRing(u * uStep, i);
		}

        //now do uvs for pipe
        uv = new Vector2[vertices.Length];


        //generate tunnel uv's
        for (int i = 0; i < (pipeSegmentCount * curveSegmentCount * 4) ; i++)
        {
            uv[i] = new Vector2(vertices[i].x, vertices[i].z);
        }
        
    }

	private void CreateFirstQuadRing (float u) {
		float vStep = (2f * Mathf.PI) / pipeSegmentCount;

		Vector3 vertexA = GetPointOnTorus(0f, 0f);
		Vector3 vertexB = GetPointOnTorus(u, 0f);
		for (int v = 1, i = 0; v <= pipeSegmentCount; v++, i += 4) {
			vertices[i] = vertexA;
			vertices[i + 1] = vertexA = GetPointOnTorus(0f, v * vStep);
			vertices[i + 2] = vertexB;
			vertices[i + 3] = vertexB = GetPointOnTorus(u, v * vStep);
		}
	}

	private void CreateQuadRing (float u, int i) {
		float vStep = (2f * Mathf.PI) / pipeSegmentCount;
		int ringOffset = pipeSegmentCount * 4;

		Vector3 vertex = GetPointOnTorus(u, 0f);
		for (int v = 1; v <= pipeSegmentCount; v++, i += 4) {
			vertices[i] = vertices[i - ringOffset + 2];
			vertices[i + 1] = vertices[i - ringOffset + 3];
			vertices[i + 2] = vertex;
			vertices[i + 3] = vertex = GetPointOnTorus(u, v * vStep);
		}
	}
    //deprecated, part of setVertices now!
    private void SetUV() {
        Vector2[] uvs = new Vector2[vertices.Length];


        //generate tunnel uv's
        for (int i = 0; i < mesh.vertices.Length - 4; i++) {
            uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
        }

        //assign u,v for each vertex we added
        uvs[mesh.vertices.Length - 4] = Vector2.zero;
        uvs[mesh.vertices.Length - 3] = Vector2.up;
        uvs[mesh.vertices.Length - 2] = Vector2.right;
        uvs[mesh.vertices.Length - 1] = Vector2.one;

        mesh.uv = uvs;
    }

    private void SetUVold () {
		uv = new Vector2[vertices.Length];
		for (int i = 0; i < vertices.Length; i+= 4) {
			uv[i] = Vector2.zero;
			uv[i + 1] = Vector2.right;
			uv[i + 2] = Vector2.up;
			uv[i + 3] = Vector2.one;
		}
		mesh.uv = uv;
	}

    private void SetTriangles() {


        triangles = new int[pipeSegmentCount * curveSegmentCount * 6];
        for (int t = 0, i = 0; t < triangles.Length; t += 6, i += 4) {
            triangles[t] = i;
            triangles[t + 1] = triangles[t + 4] = i + 2;
            triangles[t + 2] = triangles[t + 3] = i + 1;
            triangles[t + 5] = i + 3;
        }
        //bjohn: changed to set triangles for mesh1 (the pipe)
        //mesh.triangles = triangles;
        //mesh.SetTriangles(triangles, 0);

        //writeVertsToFile(mesh.vertices);

        ////NEW plan. Pre compute (on first pass) the vertices in pipe that match to vertices along
        ////our plane. What we will do is interpolate between the 4 vertices we picked. We will tesselate the plan by 20 vertices or so.
        ////parameterized for number of rows and columns. Then for each of the interpolated points, loop through all the pipe vertices (this is tedious of course)
        ////and find the closest matching point (minimum distance). Use this vertex for the plan triangles and it will "snap" to the pipe inside

        //make copy of pipeVerts
        newListLen = (pipeSegmentCount * curveSegmentCount * 4);
        numPanelVerts = 4 * (m + 1) * (n + 1);
        Vector3[] pipeVerts = new Vector3[newListLen];
        System.Array.Copy(vertices, pipeVerts, newListLen);

        //first pick 4 vertices for left side image
        bottomRightIndex = 490;
        topRightIndex = 431;
        bottomLeftIndex = 122;
        topLeftIndex = 74;

        Vector3 a = pipeVerts[bottomLeftIndex];
        Vector3 b = pipeVerts[topLeftIndex];
        Vector3 c = pipeVerts[topRightIndex];
        Vector3 d = pipeVerts[bottomRightIndex];

        int[] planeTriangles = new int[ 6*(n+1)*(m+1) ];

        //incremented by 6. Verts ordered clockwise
        int triangleIndex = 0;

        //start vertex index where pipe verts ended, incremented by 4 each loop. We are adding redundant verts.
        //uvs will be added based on this index as well
        int vertexIndex = newListLen;

        //Debug.Log(newListLen);
        //Debug.Log(mesh.vertices.Length);

        float delU = 1.0f / (n + 1.0f);
        float delV = 1.0f / (m + 1.0f);

        //variables to make adding things easier. 
        //ints are vertex indicies for LeftBottom, LeftTop,RightTop,RightBottom
        //vec2s are uv coordinates for calculating bilerp and for uv array!
        //vec3s are the points on mesh closest to respective point!
        int LB = vertexIndex, LT = vertexIndex + 1, RT = vertexIndex + 2, RB = vertexIndex + 3;
        Vector2 LBuv = Vector2.zero;
        Vector2 LTuv = new Vector2(0.0f, delV);
        Vector2 RTuv = new Vector2(delU, delV);
        Vector2 RBuv = new Vector2(delU, 0.0f);
        

        Vector3 LBpos, LTpos, RTpos, RBpos;

        //main loop. Go through each row of verts (m) and each vertical step (n) and match interpolated 
        //at each step calculate new 
        for (int j = 0; j <= m; j++)
        {
            

            for (int i = 0; i<= n; i++)
            {
                //find closest match to each of interpolated points
                LBpos = findClosest(bilerp(a, b, c, d, LBuv.x, LBuv.y), pipeVerts);
                LTpos = findClosest(bilerp(a, b, c, d, LTuv.x, LTuv.y), pipeVerts);
                RTpos = findClosest(bilerp(a, b, c, d, RTuv.x, RTuv.y), pipeVerts);
                RBpos = findClosest(bilerp(a, b, c, d, RBuv.x, RBuv.y), pipeVerts);

                vertices[LB] = LBpos;
                vertices[LT] = LTpos;
                vertices[RT] = RTpos;
                vertices[RB] = RBpos;

                uv[LB] = LBuv;
                uv[LT] = LTuv;
                uv[RT] = RTuv;
                uv[RB] = RBuv;

                planeTriangles[triangleIndex] = LB;
                planeTriangles[triangleIndex+1] = LT;
                planeTriangles[triangleIndex+2] = RT;
                planeTriangles[triangleIndex+3] = LB;
                planeTriangles[triangleIndex+4] = RT;
                planeTriangles[triangleIndex+5] = RB;

                //increment vars: int indexes, triangle index, and updating all u values for uvs
                triangleIndex += 6;
                LB += 4;
                LT += 4;
                RT += 4;
                RB += 4;

                LBuv.x += delU;
                LTuv.x += delU;
                RTuv.x += delU;
                RBuv.x += delU;
            }

            //reset uvs to left hand side, increment v's moving up a row
            LBuv.x = 0.0f;
            LTuv.x = 0.0f;

            RTuv.x = delU;
            RBuv.x = delU;

            LBuv.y += delV;
            LTuv.y += delV;
            RTuv.y += delV;
            RBuv.y += delV;
        }

        //first pick 4 vertices for left side image
        bottomRightIndex_2 = 589;
        topRightIndex_2 = 645;
        bottomLeftIndex_2 = 587;
        topLeftIndex_2 = 650;

        a = pipeVerts[bottomLeftIndex_2];
        b = pipeVerts[topLeftIndex_2];
        c = pipeVerts[topRightIndex_2];
        d = pipeVerts[bottomRightIndex_2];

        int[] planeTriangles2 = new int[6 * (n + 1) * (m + 1)];

        //incremented by 6. Verts ordered clockwise
        triangleIndex = 0;

        //for second resource need index past last resource plane
        vertexIndex = newListLen + numPanelVerts;

        //float delU = 1.0f / (n + 1.0f);
        //float delV = 1.0f / (m + 1.0f);

        //variables to make adding things easier. 
        //ints are vertex indicies for LeftBottom, LeftTop,RightTop,RightBottom
        //vec2s are uv coordinates for calculating bilerp and for uv array!
        //vec3s are the points on mesh closest to respective point!
        LB = vertexIndex;
        LT = vertexIndex + 1;
        RT = vertexIndex + 2;
        RB = vertexIndex + 3;
        LBuv = Vector2.zero;
        LTuv = new Vector2(0.0f, delV);
        RTuv = new Vector2(delU, delV);
        RBuv = new Vector2(delU, 0.0f);


        //Vector3 LBpos, LTpos, RTpos, RBpos;

        //main loop. Go through each row of verts (m) and each vertical step (n) and match interpolated 
        //at each step calculate new 
        for (int j = 0; j <= m; j++)
        {


            for (int i = 0; i <= n; i++)
            {
                //find closest match to each of interpolated points
                LBpos = findClosest(bilerp(a, b, c, d, LBuv.x, LBuv.y), pipeVerts);
                LTpos = findClosest(bilerp(a, b, c, d, LTuv.x, LTuv.y), pipeVerts);
                RTpos = findClosest(bilerp(a, b, c, d, RTuv.x, RTuv.y), pipeVerts);
                RBpos = findClosest(bilerp(a, b, c, d, RBuv.x, RBuv.y), pipeVerts);

                vertices[LB] = LBpos;
                vertices[LT] = LTpos;
                vertices[RT] = RTpos;
                vertices[RB] = RBpos;

                uv[LB] = LBuv;
                uv[LT] = LTuv;
                uv[RT] = RTuv;
                uv[RB] = RBuv;

                planeTriangles2[triangleIndex] = LB;
                planeTriangles2[triangleIndex + 1] = LT;
                planeTriangles2[triangleIndex + 2] = RT;
                planeTriangles2[triangleIndex + 3] = LB;
                planeTriangles2[triangleIndex + 4] = RT;
                planeTriangles2[triangleIndex + 5] = RB;

                //increment vars: int indexes, triangle index, and updating all u values for uvs
                triangleIndex += 6;
                LB += 4;
                LT += 4;
                RT += 4;
                RB += 4;

                LBuv.x += delU;
                LTuv.x += delU;
                RTuv.x += delU;
                RBuv.x += delU;
            }

            //reset uvs to left hand side, increment v's moving up a row
            LBuv.x = 0.0f;
            LTuv.x = 0.0f;

            RTuv.x = delU;
            RBuv.x = delU;

            LBuv.y += delV;
            LTuv.y += delV;
            RTuv.y += delV;
            RBuv.y += delV;
        }


        //finalize
        mesh.vertices = vertices;
        mesh.uv       = uv;

        mesh.SetTriangles(triangles, 0);
        mesh.SetTriangles(planeTriangles, 1);
        mesh.SetTriangles(planeTriangles2, 2);

        //If we can't support two (also might not have enough content)
        //we can do one per pipe ( and have live video floating or on sides)

    }

	private Vector3 GetPointOnTorus (float u, float v) {
		Vector3 p;
		float r = (curveRadius + pipeRadius * Mathf.Cos(v));
		p.x = r * Mathf.Sin(u);
		p.y = r * Mathf.Cos(u);
		p.z = pipeRadius * Mathf.Sin(v);
		return p;
	}

	public void AlignWith (Pipe pipe) {
		//relativeRotation = Random.Range(0, curveSegmentCount) * 360f / pipeSegmentCount;

        relativeRotation = 0f;
		transform.SetParent(pipe.transform, false);
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.Euler(0f, 0f, -pipe.curveAngle);
		transform.Translate(0f, pipe.curveRadius, 0f);
		transform.Rotate(relativeRotation, 0f, 0f);
		transform.Translate(0f, -curveRadius, 0f);
		transform.SetParent(pipe.transform.parent);
		transform.localScale = Vector3.one;
	}

    public void OnDrawGizmos()
    {
        //Gizmos.color = Color.white;
        //float radius = .1f;

        //var transform = this.transform;
        //Gizmos.DrawWireSphere(transform.TransformPoint(vertices[bottomLeftIndex]), radius);
        //Gizmos.color = Color.red;
        //Gizmos.DrawWireSphere(transform.TransformPoint(vertices[topLeftIndex]), radius);
        //Gizmos.color = Color.green;
        //Gizmos.DrawWireSphere(transform.TransformPoint(vertices[bottomRightIndex]), radius);
        //Gizmos.color = Color.blue;
        //Gizmos.DrawWireSphere(transform.TransformPoint(vertices[topRightIndex]), radius);
    }
}